# Moved to GitHub

This module migrated to https://github.com/atlassian/aws-infrastructure due to [JPERF-375](https://ecosystem.atlassian.net/browse/JPERF-375).